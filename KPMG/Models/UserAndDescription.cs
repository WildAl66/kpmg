﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KPMG.Models
{
    public class UserAndDescription
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string Description { get; set; }
    }
}
