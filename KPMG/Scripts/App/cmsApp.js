﻿angular.module("cmsApp", ['ui.router'])
    .config(function($stateProvider, $urlRouterProvider) {
    
        $urlRouterProvider.otherwise('/home');
    
        $stateProvider
        
            // HOME STATES AND NESTED VIEWS ========================================
            .state('home', {
                url: '/home',
                templateUrl: 'partial-home.html',
                controller: 'homeController'
            })
            .state('create', {
                url: '/create',
                templateUrl: 'partial-create.html',
                controller: 'createController'
            }).state('read', {
                url: '/read',
                templateUrl: 'partial-read.html',
                controller: 'readController'
            }).state('mostPopular', {
                url: '/mostPopular',
                templateUrl: 'partial-most-popular.html',
                controller: 'mostPopularController'
            }).state('admin', {
                url: '/admin',
                templateUrl: 'partial-admin.html',
                controller: 'adminController'
            })
    })

.factory('adminFactory', ['$http', function ($http) {

    var adminFactory = {};
    var defaultUrl = 'api/admin';

    adminFactory.getUsers = function () {
        return $http.get(defaultUrl);
    }

    adminFactory.dataAdmin = function (id) {
        return $http.post(defaultUrl + '/' + id);
    }

    return adminFactory;

    }
])

.factory('articleFactory', ['$http', function ($http) {

    var articleFactory = {};
    var defaultUrl = 'api/articles';

    var createArticle = function (title, description, body, authorId) {
        return { Title: title, Description: description, Body: body, AuthorId: authorId }
    };

    articleFactory.getArticles = function () {
        return $http.get(defaultUrl);
    };

    
    articleFactory.addArticle = function (newArticle) {
        
        return $http.post(defaultUrl, newArticle);
    }

    return articleFactory;

}])

.factory('commentFactory', ['$http', function ($http) {

    var commentFactory = {};
    var defaultUrl = 'api/comments';


    commentFactory.addComment = function (newComment) {
        return $http.post(defaultUrl, newComment);
    };

    return commentFactory;

}])

.service('adminService', [function () {

    this.currentUser = {};

    this.setCurrentUser = function (selectedUser) {
        this.currentUser = selectedUser;
        console.log('user selected is ' + selectedUser.UserName);
    };

}])


.controller("homeController", ['$scope', 'adminFactory', 'adminService',  function ($scope, adminFactory, adminService) {
    $scope.welcome = "Welcome to Pressford CMS ";
    $scope.message = "";

    $scope.users = [];
    $scope.selectedUser = {};

    adminFactory.getUsers().success(function (data) {
        $scope.users = data;
    })
    .error(function(err){
        $scope.message = err;
    });


    $scope.setUser = function (selectedUser) {
        adminService.setCurrentUser(selectedUser);
    };

    $scope.getCurrentUser = function () {
        return adminService.currentUser;
    };

}])

.controller("createController", ['$scope', 'articleFactory', 'adminService', function ($scope, articleFactory, adminService) {
    $scope.welcome = "Write an article";
    $scope.message = "";

    $scope.newArticle = {};


    $scope.addArticle = function () {
        $scope.newArticle.AuthorId = adminService.currentUser.UserId;

        articleFactory.addArticle($scope.newArticle).success(function (data) {
            $scope.message = "Article Added";
        })
        .error(function (err) {
            $scope.message = err;
        })
    }
}])


.controller("readController", ['$scope', '$sce', 'articleFactory', 'commentFactory', 'adminService', function ($scope, $sce, articleFactory, commentFactory, adminService) {
    $scope.welcome = "Read articles and comment";
    $scope.message = "";

    $scope.articles = [];

    articleFactory.getArticles().success(function (data) {
        $scope.articles = data;
    })
    .error(function (err) {
        $scope.message = err;
    });


    $scope.renderHtml = function (html_code) {
        return $sce.trustAsHtml(html_code);
    };

    //comments section
    $scope.newCommentMode = false;

    $scope.newComment = {};

    $scope.addComment = function () {
        $scope.newCommentMode = true;
    }

    $scope.saveComment = function () {
        commentFactory.addComment(newComment).success(function (data) {
            $scope.newComment.AuthorId = adminService.currentUser.UserId;
            $scope.message = "Comment Added";
            $scope.newCommentMode = false;
        })
        .error(function (err) {
            $scope.message = err;
        });
    }
    
}])
.controller("mostPopularController", ['$scope', function ($scope) {
    $scope.welcome = "Most popular content";
    $scope.message = "";
}])


.controller("adminController", ['$scope', 'adminFactory', function ($scope, adminFactory) {
    $scope.welcome = "Admin functions";
    $scope.message = "";

    $scope.dataAdmin = function (actionType) {
        adminFactory.dataAdmin(actionType);
    };
}]);
