﻿using KPMG.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace KPMG.Controllers
{
    public class AdminController : ApiController
    {
        private KPMGEntities db = new KPMGEntities();

        // GET: api/Articles
        public IEnumerable<UserAndDescription> GetUsers()
        {
            return db.Users.AsEnumerable().Select(x => new UserAndDescription
            {
                UserId = x.UserId,
                UserName = x.UserName,
                Description = GetDescription(x)
            });            
        }

        private string GetDescription(User user)
        {
            if (user.Roles.Count(x => x.RoleName == "Admin") > 0)
                return "Admin";

            if (user.Roles.Count(x => x.RoleName == "Publisher") > 0)
                return "Publisher";

            return "User";
        }
        // POST: api/Admin/id
        [ResponseType(typeof(string))]
        [HttpPost]
        public string DataAdmin(int id)
        {
            string retStr = "";
            switch (id)
            {
                case 0:
                    ClearDown();
                    break;
                case 1:
                    LoadData();
                    break;
                case 2:
                    ClearDown();
                    LoadData();
                    break;
            }

            return retStr;
        }

        public void ClearDown()
        {
            
            foreach(Comment c in db.Comments)
            {
                db.Comments.Remove(c);
            }
            db.SaveChanges();

            foreach(Article a in db.Articles)
            {
                //article likes
                foreach(var al in a.Users.ToList())
                {
                    a.Users.Remove(al);
                }
                db.Articles.Remove(a);
            }
            db.SaveChanges();

            foreach(User u in db.Users)
            {
                //user roles
                foreach (var r in u.Roles.ToList())
                {
                    u.Roles.Remove(r);
                }
                db.Users.Remove(u);
            }
            db.SaveChanges();

            foreach(Role r in db.Roles)
            {
                db.Roles.Remove(r);
            }
            db.SaveChanges();
        }

        public void LoadData()
        {
            //roles
            Role adminRole = new Role();
            adminRole.RoleName = "Admin";
            db.Roles.Add(adminRole);

            Role userRole = new Role();
            userRole.RoleName = "User";
            db.Roles.Add(userRole);

            Role publisherRole = new Role();
            publisherRole.RoleName = "Publisher";
            db.Roles.Add(publisherRole);

            db.SaveChanges();

            //users
            User viewer = new User();
            viewer.UserName = "Howard";
            db.Users.Add(viewer);
            db.SaveChanges();

            viewer.Roles.Add(userRole);
            db.SaveChanges();

            User publisher = new Models.User();
            publisher.UserName = "Mrs Jones";
            db.Users.Add(publisher);
            db.SaveChanges();

            publisher.Roles.Add(publisherRole);
            db.SaveChanges();

            //articles
            for (int i = 0; i < 5; i++)
            {
                Article article = new Article();
                article.Title = "Sample article " + i;
                article.Description = "Test the view for article " + i;
                article.PublishDate = System.DateTime.UtcNow;
                article.Body = "<h2>Article first heading</h2><p>Some detailed content etc etc etc</p>";
                article.AuthorId = publisher.UserId;
                db.Articles.Add(article);
                db.SaveChanges();

                //article like
                switch (i)
                {
                    case 2:
                        article.Users.Add(publisher);
                        article.Users.Add(viewer);
                        db.SaveChanges();
                        break;
                    case 4:
                        article.Users.Add(publisher);
                        db.SaveChanges();
                        break;
                }
            }
        }
    }
}
